using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelMenu : MonoBehaviour
{
    public void levelOne()
    {
        SceneManager.LoadScene("StaggerBugs_1");
        // PlayerPrefs.SetInt("index",11);
    }
    public void levelTwo()
    {
        SceneManager.LoadScene("StaggerBugs_2");
        // PlayerPrefs.SetInt("index",11);
    }
    public void levelThree()
    {
        SceneManager.LoadScene("StaggerBugs_3");
        // PlayerPrefs.SetInt("index",11);
    }
    public void levelFour()
    {
        SceneManager.LoadScene("StaggerBugs_4");
        // PlayerPrefs.SetInt("index",11);
    }
    public void Back()
    {
        SceneManager.LoadScene("MainMenu");
    }

    /*
    public void ChangeCharacter()
    {
        SceneManager.LoadScene("ChangeCharacter");
    }
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    */
}
