using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
    public void PlayGame()
    {
        SceneManager.LoadScene("LevelMenu");
        // PlayerPrefs.SetInt("index",11);
    }
    public void ChangeCharacter()
    {
        SceneManager.LoadScene("ChangeCharacter");
    }
    public void QuitTheGame()
    {
        Application.Quit();
    }
    public void Back()
    {
        SceneManager.LoadScene("MainMenu");
    }
}
