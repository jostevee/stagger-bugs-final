using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
public class CharacterManager : MonoBehaviour
{
    public CharacterDatabase characterDB;

    public Text nameText;
    public SpriteRenderer artworkSprite;
    private int selectedOption = 0;
    
    // Start is called before the first frame update
    void Start()
    {
       if(!PlayerPrefs.HasKey("index"))
       {
        selectedOption = 0;
       }
       else
       {
        Load();
       }
       UpdateCharacter(selectedOption);
    }
    private void Update()
    {
        if (Input.GetKeyUp(KeyCode.LeftArrow))
        {
            PreviousOption();
        }
        if (Input.GetKeyUp(KeyCode.RightArrow))
        {
            NextOption();
        }
        if (Input.GetKeyUp(KeyCode.Return)){
            SaveAndBack();
        }
        if (Input.GetKeyUp(KeyCode.Escape)){
            Back();
        }
    }
    
    public void NextOption()
    {
        selectedOption++;
        if(selectedOption >= characterDB.CharacterCount)
        {
            selectedOption = 0;
        }
        UpdateCharacter(selectedOption);
    }
    public void PreviousOption()
    {
        if(selectedOption == 0)
        {
            selectedOption = characterDB.CharacterCount-1;
        }else{
            selectedOption--;
        }
        UpdateCharacter(selectedOption);

    }
    private void UpdateCharacter(int selectedOption)
    {
        Character character = characterDB.GetCharacter(selectedOption);
        artworkSprite.sprite = character.characterSprite;
        nameText.text = character.characterName;
    }
    private void Load()
    {
        selectedOption = PlayerPrefs.GetInt("index");
    }
    private void Save()
    {
        PlayerPrefs.SetInt("index", selectedOption);
    }
    public void SaveAndBack()
    {
        Save();
        SceneManager.LoadScene("MainMenu");
    }

    public void Back()
    {
        SceneManager.LoadScene("MainMenu");
    }
    // Update is called once per frame
}
