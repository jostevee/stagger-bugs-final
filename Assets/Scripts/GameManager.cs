using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public Player player;

    public GameObject[] players, hearts;

    public Text scoreText;

    public GameObject playButton, backButton;

    public GameObject gameOver;

    private int score, healthNow = 3;

    private void Awake()
    {
        Application.targetFrameRate = 60;
        gameOver.SetActive(false);

        // Pause();
    }

    public void Play()
    {
        for (int i = 0; i < players.Length; i++)
        {
            if(i == PlayerPrefs.GetInt("index",0)) {
                players[i].SetActive(true);
                player = players[i].GetComponent<Player>();
            } else if(i != PlayerPrefs.GetInt("index",0)) {
                players[i].SetActive(false);
            }
        }

        healthNow = 3;
        ResetHealth();

        score = 0;
        scoreText.text = score.ToString();

        backButton.SetActive(false);
        playButton.SetActive(false);
        gameOver.SetActive(false);

        Time.timeScale = 1f;
        player.enabled = true;

        Pipes[] pipes = FindObjectsOfType<Pipes>();

        for(int i=0; i<pipes.Length; i++){
            Destroy(pipes[i].gameObject);
        }
    }
    public void BackToMenu()
    {
        SceneManager.LoadScene("LevelMenu");
    }

    public void Pause()
    {
        Time.timeScale = 0f;
        player.enabled = false;
    }

    public void GameOver()
    {
        // Debug.Log("Game Over");
        gameOver.SetActive(true);
        playButton.SetActive(true);
        backButton.SetActive(true);

        Pause();
    }

    public void IncreaseScore()
    {
        score++;
        scoreText.text = score.ToString();
    }

    public void DecreaseHealth()
    {
        healthNow--;
        hearts[healthNow].SetActive(false);
        if(healthNow==0)
        {
            GameOver();
        }
    }

    private void ResetHealth()
    {
        for(int i = 0;i < healthNow; i++)
        {
            if(!hearts[i].activeSelf)
            {
                hearts[i].SetActive(true);
            }
        }
    }

    // // Start is called before the first frame update
    // void Start()
    // {
        
    // }

    // // Update is called once per frame
    // void Update()
    // {
        
    // }
}
